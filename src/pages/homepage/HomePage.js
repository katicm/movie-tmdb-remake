import React, { Component } from "react";
import HomeParts from "../../components/home-parts/HomeParts";

class HomePage extends Component {
  render() {
    return (
      <div>
        <HomeParts />
      </div>
    );
  }
}
export default HomePage;
