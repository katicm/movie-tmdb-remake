import React from "react";
import PersonPanel from "../../components/person-details/PersonPanel";
import { useParams } from "react-router-dom";

const PersonPage = () => {
  const { personID } = useParams();
  return <PersonPanel id={personID} />;
};
export default PersonPage;
