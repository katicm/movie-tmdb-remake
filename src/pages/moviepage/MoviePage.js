import React from "react";
import { useParams } from "react-router-dom";
import MoviePanel from "../../components/movie-details/MoviePanel";

const MoviePage = () => {
  const { titleID } = useParams();
  return (
    <div>
      <MoviePanel id={titleID} />
    </div>
  );
};
export default MoviePage;
