import React from "react";
import WatchlistDetails from "../../components/watchlist/WatchlistDetails";

const WatchlistPage = () => {
  return (
    <div>
      <WatchlistDetails />
    </div>
  );
};
export default WatchlistPage;
