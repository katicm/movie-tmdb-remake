import React from "react";
import numeral from "numeral";

const MoviesDetails = ({ movie }) => {
  return (
    <React.Fragment>
      <div className="title">{movie.title}</div>
      <div className="genres">
        {movie.genres.map((item, i) => (
          <p key={i}>{item}</p>
        ))}
      </div>
      <div className="info">
        <div>
          Runtime:
          <br /> {movie.runtime} min
        </div>
        <div>
          Release Date:
          <br />
          {movie.release_date}
        </div>
        <div>
          Status:
          <br />
          {movie.status}
        </div>
        <div>
          Budget:
          <br />
          {numeral(movie.budget).format("$ 0 a")}
        </div>
        <div>
          Revenue:
          <br />
          {numeral(movie.revenue).format("$ 0.00 a")}
        </div>
      </div>
      <div className="overview">{movie.overview}</div>
    </React.Fragment>
  );
};
export default MoviesDetails;
