import React, { useState, useEffect } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import not_found from "../../images/not_found_res.png";
import arrow from "../../images/arrow_menu.png";

const CreditsDetails = ({ id, param }) => {
  const [data, setData] = useState({ cast: [], crew: [] });
  const [director, setDirector] = useState(false);
  const [type] = useState(param.type);
  const [open, setOpen] = useState(false);
  const [redirect, setRedirect] = useState(null);

  useEffect(() => {
    const source = axios.CancelToken.source();
    const fetchData = async () => {
      try {
        await axios
          .get(
            `https://api.themoviedb.org/3/${type}/${id}/credits?api_key=${process.env.REACT_APP_TMDB_API_KEY}`,
            { cancelToken: source.token }
          )
          .then(result => {
            setData(result.data);
            setDirector(
              result.data.crew.find(director => director.job === "Director")
            );
          });
      } catch (error) {
        if (axios.isCancel(error)) {
        }
      }
    };
    fetchData();
    return () => {
      source.cancel();
      //cancel asynchronous tasks, error - cant set state on an unmounted component
    };
  }, [id, type]);
  const cast = open ? 20 : 5;

  const handleRedirect = id => {
    setRedirect(`/person/${id}`);
  };

  if (redirect) return <Redirect push to={redirect} />;
  return (
    <div className="credits">
      {director && (
        <div onClick={() => handleRedirect(director.id)} className="info">
          <img
            src={
              director.profile_path !== null
                ? "https://image.tmdb.org/t/p/w185" + director.profile_path
                : not_found
            }
            alt="X"
          />
          {director.name} <p>Director</p>
        </div>
      )}
      {open &&
        data.crew.slice(0, 6).map(item => (
          <div
            onClick={() => handleRedirect(item.id)}
            key={item.id + item.job}
            className="info"
          >
            {item.profile_path !== null ? (
              <img
                src={"https://image.tmdb.org/t/p/w185" + item.profile_path}
                alt="X"
              />
            ) : (
              <img style={{ height: "225px" }} src={not_found} alt="X" />
            )}
            {item.name}
            <p>{item.job}</p>
          </div>
        ))}
      {data.cast.slice(0, cast).map(item => (
        <div
          onClick={() => handleRedirect(item.id)}
          key={item.id}
          className="info"
        >
          {item.profile_path !== null ? (
            <img
              src={"https://image.tmdb.org/t/p/w185" + item.profile_path}
              alt="X"
            />
          ) : (
            <img style={{ height: "225px" }} src={not_found} alt="X" />
          )}
          {item.name}
          <p>as {item.character}</p>
        </div>
      ))}
      <div onClick={() => setOpen(open => !open)} className="more">
        <p>
          See {open ? "less" : "full"}
          <br /> cast and crew
        </p>
        <img
          style={{
            transform: open && "rotate(180deg)"
          }}
          src={arrow}
          alt=">>"
        />
        <img
          style={{
            top: "-105px",
            left: "20px",
            transform: open && "rotate(180deg)"
          }}
          src={arrow}
          alt=">>"
        />
      </div>
    </div>
  );
};
const mapStateToProps = ({ movie }) => ({
  param: movie.searchParameters,
  id: movie.movie.id
});
export default connect(mapStateToProps)(CreditsDetails);
