import React, { useState, useEffect } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { setSearchParameters } from "../../redux/movie/movie.action";
import { getDataToWatchlist, checkWatchlist } from "../../movieAPI/actions";

const SimilarMovies = ({ movie, setSearchParameters, user, watchlist }) => {
  const [data, setData] = useState([]);
  const [focused, setFocused] = useState(0);
  const { id } = movie;
  const [type] = useState(movie.type === "series" ? "tv" : movie.type);

  useEffect(() => {
    const source = axios.CancelToken.source();
    const fetchData = async () => {
      try {
        await axios
          .get(
            `https://api.themoviedb.org/3/${type}/${id}/recommendations?api_key=${process.env.REACT_APP_TMDB_API_KEY}`,
            { cancelToken: source.token }
          )
          .then(result => {
            setData(result.data.results);
          });
      } catch (error) {
        if (axios.isCancel(error)) {
        }
      }
    };
    fetchData();
    return () => {
      source.cancel();
      //cancel asynchronous tasks, error - cant set state on an unmounted component
    };
  }, [id, type]);

  const handleWatchlist = add => {
    if (user && !checkWatchlist({ id: data[focused].id, type }, watchlist))
      getDataToWatchlist(user.id, data[focused], type, add);
  };

  return (
    <div className="similar">
      <div className="list">
        {data.slice(0, 8).map((item, i) => (
          <div
            className={focused === i ? "item_focus" : undefined}
            onClick={() => setFocused(i)}
            key={item.id}
          >
            <img
              src={"https://image.tmdb.org/t/p/w185" + item.poster_path}
              alt="Poster Not Found"
            />
          </div>
        ))}
      </div>
      {data.length !== 0 && (
        <div className="focused">
          <div>
            <img
              src={
                "https://image.tmdb.org/t/p/w342" + data[focused].poster_path
              }
              alt="Poster Not Found"
            />
            <Link to={user ? `/title/${data[focused].id}` : "/login"}>
              <button onClick={() => handleWatchlist(true)}>
                Add to Watchlist
              </button>
            </Link>
          </div>
          <div className="info">
            <div className="title">
              <Link
                onClick={() =>
                  setSearchParameters({
                    name: type === "tv" ? "TV Series" : "Movies",
                    type
                  })
                }
                to={`/title/${data[focused].id}`}
              >
                {data[focused].original_title === undefined
                  ? data[focused].original_name
                  : data[focused].original_title}
              </Link>
            </div>
            <div className="date">
              {data[focused].original_title === undefined
                ? data[focused].first_air_date.split("-")[0]
                : data[focused].release_date.split("-")[0]}
            </div>
            <div className="overview">{data[focused].overview}</div>
          </div>
        </div>
      )}
    </div>
  );
};
const mapStateToProps = state => ({
  movie: state.movie.movie,
  user: state.user.currentUser,
  watchlist: state.watchlist.watchlist
});
const mapDispatchToProps = dispatch => ({
  setSearchParameters: data => dispatch(setSearchParameters(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(SimilarMovies);
