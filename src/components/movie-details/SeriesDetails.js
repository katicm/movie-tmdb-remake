import React from "react";

const SeriesDetails = ({ series }) => {
  return (
    <React.Fragment>
      <div className="title">{series.title}</div>
      <div className="genres">
        {series.genres.map(item => (
          <p key={item}>{item}</p>
        ))}
      </div>
      <div className="info">
        <div>
          Runtime: <br />
          {series.runtime} min
        </div>
        <div>
          Air date:
          <br /> {series.air_date}
        </div>
        <div>
          Seasons: <br />
          {series.seasons_num}
        </div>
        <div>
          Episodes:
          <br />
          {series.episodes_num}
        </div>
        <div>
          Last episode:
          <br /> {series.last_episode}
        </div>
        <div>
          Ended: <br />
          {series.ended ? "No" : "Yes"}
        </div>
      </div>
      <div className="overview">{series.overview}</div>
    </React.Fragment>
  );
};
export default SeriesDetails;
