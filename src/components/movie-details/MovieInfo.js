import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { updateWatchlist, checkWatchlist } from "../../movieAPI/actions";
import MovieDetails from "./MovieDetails";
import SeriesDetails from "./SeriesDetails";
import star from "../../images/star.png";
import check from "../../images/check.png";
import not_found from "../../images/not_found.png";

const MovieInfo = ({ movie, watchlist, user }) => {
  const [inWatchlist, setInWatchlist] = useState(false);

  useEffect(() => {
    document.title = movie.title + " - KMDb";
    if (movie.backdrop !== null) {
      setTimeout(() => {
        window.scrollTo({ top: 500, behavior: "smooth" });
      }, 500);
    }
    setInWatchlist(checkWatchlist(movie, watchlist));
  }, [movie, watchlist]);

  const handleWatchlist = add => {
    if (user) {
      if (add) {
        updateWatchlist(user.id, { ...movie, added: new Date() }, add);
      } else {
        const toRemove = watchlist.find(
          item => item.id === movie.id && item.type === movie.type
        );
        if (user) updateWatchlist(user.id, toRemove, add);
      }
    }
  };

  return (
    <React.Fragment>
      <div className="cover">
        {movie.backdrop && (
          <img
            src={"https://image.tmdb.org/t/p/w1280/" + movie.backdrop}
            alt="A"
          />
        )}
      </div>
      <div className="movie">
        {movie.poster !== null ? (
          <img
            src={"https://image.tmdb.org/t/p/w342" + movie.poster}
            className="poster"
            alt="X"
          />
        ) : (
          <img className="poster" src={not_found} alt="X" />
        )}
        <div className="movie_details">
          <div className="add_watchlist">
            <div className="rating">
              {movie.rating}/10
              <img src={star} alt="X"></img>
            </div>
            {inWatchlist ? (
              <button
                className="remove_watchlist"
                onClick={() => handleWatchlist(false)}
                hover="Remove from Watchlist"
              >
                <span>
                  <img src={check} alt="X" />
                  Watchlist
                </span>
              </button>
            ) : (
              <button onClick={() => handleWatchlist(true)}>
                {!user ? (
                  <Link to="/login">Add to Watchlist</Link>
                ) : (
                  <div>Add to Watchlist</div>
                )}
              </button>
            )}
          </div>
          {movie.type !== "series" ? (
            <MovieDetails movie={movie} />
          ) : (
            <SeriesDetails series={movie} />
          )}
        </div>
      </div>
    </React.Fragment>
  );
};
const mapStateToProps = state => ({
  movie: state.movie.movie,
  watchlist: state.watchlist.watchlist,
  user: state.user.currentUser
});
export default connect(mapStateToProps)(MovieInfo);
