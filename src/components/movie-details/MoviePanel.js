import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { fetchMovie } from "../../redux/movie/movie.action";
import CreditsDetails from "./CreditsDetails";
import SimilarMovies from "./SimilarMovies";
import MovieInfo from "./MovieInfo";
import Loading from "../loading/Loading";
import "./movie.style.scss";

const MoviePanel = props => {
  const [parameters] = useState(props.movie.searchParameters);
  const {
    id,
    fetchMovie,
    movie: { isLoading, error }
  } = props;

  useEffect(() => {
    const fetchData = () => {
      fetchMovie(parameters.type, id);
    };
    fetchData();
  }, [id, fetchMovie, parameters]);

  if (error) {
    return <h1 className="container-error">404 PAGE NOT FOUND</h1>;
  } else
    return (
      <div className="container">
        {!isLoading ? (
          <React.Fragment>
            <MovieInfo />
            <div className="segment">Credits:</div>
            <CreditsDetails />
            <div className="segment">More To Love:</div>
            <SimilarMovies />
          </React.Fragment>
        ) : (
          <Loading />
        )}
      </div>
    );
};
const mapStateToProps = state => ({
  movie: state.movie
});
const mapDispatchToProps = dispatch => ({
  fetchMovie: (type, id) => dispatch(fetchMovie(type, id))
});
export default connect(mapStateToProps, mapDispatchToProps)(MoviePanel);
