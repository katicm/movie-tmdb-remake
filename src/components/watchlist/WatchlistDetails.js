import React, { Component } from "react";
import { connect } from "react-redux";
import WatchlistItem from "./WatclistItem";
import "./watchlist.style.scss";

class WatchlistDetails extends Component {
  componentDidMount() {
    document.title = "Your Watchlist - KMDb";
  }
  handleRate = () => {};

  render() {
    const { watchlist } = this.props;
    return (
      <div className="container_watchlist">
        <div className="watchlist_title">Your Watchlist: </div>
        {watchlist.map((item, i) => (
          <div key={i} className="movie_item">
            <WatchlistItem item={item} />
          </div>
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  watchlist: state.watchlist.watchlist
});
export default connect(mapStateToProps)(WatchlistDetails);
