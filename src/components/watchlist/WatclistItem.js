import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { updateWatchlist } from "../../movieAPI/actions";
import star from "../../images/star.png";
import star_blue from "../../images/star_blue.png";
import remove from "../../images/remove.png";
import MovieDetails from "../movie-details/MovieDetails";
import SeriesDetails from "../movie-details/SeriesDetails";
import Ratings from "react-ratings-declarative";

const WatchlistItem = ({ item, user }) => {
  const [rateIt, setRateIt] = useState(false);

  useEffect(() => {
    setRateIt(item.user_rating === 0 ? false : true);
  }, [item]);

  const handleClick = () => {
    setRateIt(!rateIt);
  };
  const handleRate = newRating => {
    //todo
    updateWatchlist(user.id, item, false);
    updateWatchlist(user.id, { ...item, user_rating: newRating }, true);
    setRateIt(false);
  };

  return (
    <React.Fragment>
      <div className="poster">
        <img
          src={"https://image.tmdb.org/t/p/w185" + item.poster}
          alt="Loading..."
        />
      </div>
      <div className="center_panel">
        {item.type === "series" ? (
          <SeriesDetails series={item} />
        ) : (
          <MovieDetails movie={item} />
        )}
      </div>
      <div className="right_panel">
        <div
          onClick={() => updateWatchlist(user.id, item, false)}
          className="remove"
        >
          <img src={remove} alt="X" />
        </div>
        {!rateIt ? (
          <div className="personal">
            <div className="personal_rate">Rate This</div>
            <Ratings
              rating={item.user_rating}
              changeRating={handleRate}
              widgetRatedColors="#4268f1"
              widgetHoverColors="#4268f1"
              widgetEmptyColors="grey"
              widgetDimensions="15px"
              widgetSpacings="1px"
            >
              {[...Array(10)].map((e, i) => (
                <Ratings.Widget key={i} />
              ))}
            </Ratings>
          </div>
        ) : (
          <div className="rating">
            {item.user_rating}/10
            <img onClick={handleClick} src={star_blue} alt="X" />
          </div>
        )}
        <div className="rating">
          {item.rating}/10
          <img src={star} alt="X"></img>
        </div>
      </div>
    </React.Fragment>
  );
};
const mapStateToProps = state => ({
  user: state.user.currentUser
});
export default connect(mapStateToProps)(WatchlistItem);
