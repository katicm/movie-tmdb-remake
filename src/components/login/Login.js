import React, { Component } from "react";
import SignIn from "./SignIn";
import SignUp from "./SignUp";

import "./login.style.scss";

class Login extends Component {
  state = { sign_in_form: true };

  componentDidMount() {
    document.title = "Sign In - KMDb";
  }

  handleClick = () => {
    this.setState(prevState => ({ sign_in_form: !prevState.sign_in_form }));
    document.title = this.state.sign_in_form
      ? "Sign Up - KMDb"
      : "Sign In - KMDb";
  };
  render() {
    return (
      <div>
        <div className="login">
          <SignIn
            className={`${this.state.sign_in_form ? "" : "in-hide"} form`}
          ></SignIn>
          <SignUp
            className={`${this.state.sign_in_form ? "up-hide" : ""} form`}
          ></SignUp>
        </div>
        <div className="login">
          <div
            className={`${this.state.sign_in_form ? "in-hide" : ""} swaper_l`}
          >
            <h1 className="h1_swap">Welcome Back!</h1>
            <p>
              If you already have account, please sign in with your user info
            </p>
            <button
              onClick={this.handleClick}
              className="form_button swaper_l_button"
            >
              Sign in
            </button>
          </div>
          <div
            className={`${this.state.sign_in_form ? "" : "up-hide"} swaper_r`}
          >
            <h1 className="h1_swap">Hello, Friend!</h1>
            <p>To explore full potential of site please sign up</p>
            <button
              onClick={this.handleClick}
              className="form_button swaper_r_button"
            >
              Sign up
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default Login;
