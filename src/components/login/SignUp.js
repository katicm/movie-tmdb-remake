import React, { Component } from "react";
import InputField from "../../components/input-field/InputField";
import { auth, createUserProfileDocument } from "../../firebase/firebase.utils";

class SignUp extends Component {
  state = {
    email: "",
    username: "",
    password: "",
    repeat_password: "",
    error: ""
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = async e => {
    e.preventDefault();
    this.setState({ error: "" });
    const { username, email, password, repeat_password } = this.state;
    if (password !== repeat_password) {
      this.setState({ error: "Passwords do not match." });
      return;
    }
    try {
      const { user } = await auth.createUserWithEmailAndPassword(
        email,
        password
      );
      await createUserProfileDocument({ ...user, displayName: username });
    } catch (error) {
      this.setState({ error: error.message });
    }
  };
  render() {
    const { email, username, password, repeat_password, error } = this.state;
    return (
      <div className={this.props.className}>
        <h1>Create Account</h1>
        <form onSubmit={this.handleSubmit}>
          <InputField
            handleChange={this.handleChange}
            label="email"
            value={email}
            name="email"
            type="email"
            required
          />
          <InputField
            handleChange={this.handleChange}
            label="username"
            value={username}
            name="username"
            type="text"
            required
          />
          <InputField
            handleChange={this.handleChange}
            label="password"
            value={password}
            name="password"
            type="password"
            required
          />
          <InputField
            handleChange={this.handleChange}
            label="repeat password"
            value={repeat_password}
            name="repeat_password"
            type="password"
            required
          />
          {error !== "" && <div>{this.state.error}</div>}
          <button className="form_button" type="submit">
            Sign up
          </button>
        </form>
      </div>
    );
  }
}
export default SignUp;
