import React, { Component } from "react";
import InputField from "../input-field/InputField";
import { auth, signInWithGoogle } from "../../firebase/firebase.utils";

class SignIn extends Component {
  state = { email: "", password: "", error: "" };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = async e => {
    e.preventDefault();
    this.setState({ error: "" });
    const { email, password } = this.state;
    try {
      await auth.signInWithEmailAndPassword(email, password);
      this.setState({ email: "", password: "" });
    } catch (error) {
      this.setState({ error: error.message });
    }
  };
  render() {
    const { email, password, error } = this.state;
    return (
      <div className={this.props.className}>
        <h1>Sign in</h1>
        <form onSubmit={this.handleSubmit}>
          <InputField
            handleChange={this.handleChange}
            label="email"
            value={email}
            name="email"
            type="email"
            required
          />
          <InputField
            handleChange={this.handleChange}
            label="password"
            value={password}
            name="password"
            type="password"
            required
          />
          {error !== "" && <div>{this.state.error}</div>}
          <button className="form_button" type="submit">
            Sign in
          </button>
        </form>
        <div className="divider">
          <div>OR</div>
        </div>
        <button
          style={{ background: "#4285f4", marginTop: "18px" }}
          className="form_button"
          onClick={signInWithGoogle}
        >
          Sign in with Google
        </button>
      </div>
    );
  }
}
export default SignIn;
