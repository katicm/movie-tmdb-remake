import React from "react";
import { connect } from "react-redux";
import not_found from "../../images/not_found.png";
import female from "../../images/female.png";
import male from "../../images/male.png";
import PersonSocial from "./PersonSocial";

const PersonDetails = ({ person }) => {
  const {
    biography,
    birthday,
    profile_path,
    name,
    place_of_birth,
    gender,
    deathday,
    facebook_id,
    imdb_id,
    instagram_id,
    twitter_id
  } = person;
  const social = { facebook_id, imdb_id, instagram_id, twitter_id };

  return (
    <div className="person-details">
      <div className="person-profile">
        <img
          src={
            profile_path === null
              ? not_found
              : "https://image.tmdb.org/t/p/w342/" + profile_path
          }
          alt={not_found}
        />
        <div className="person-birth-place">
          Born: {birthday}
          <p>Birth Place:</p>
          {place_of_birth}
          <br />
          {deathday && `Died: ${deathday}`}
        </div>
        <PersonSocial social={social} />
      </div>
      <div className="person-info">
        <div className="person-title">
          {name}
          <img src={gender === 1 ? female : male} alt="F" />
        </div>
        <p>Biography</p>
        <div className="person-biography">{biography}</div>
      </div>
    </div>
  );
};
const mapStateToProps = state => ({
  person: state.person.person
});
export default connect(mapStateToProps)(PersonDetails);
