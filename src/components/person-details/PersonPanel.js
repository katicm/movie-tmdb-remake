import React, { useEffect } from "react";
import PersonDetails from "./PersonDetails";
import Loading from "../loading/Loading";
import { connect } from "react-redux";
import { fetchPerson } from "../../redux/person/person.action";
import "./person.style.scss";

const PersonPanel = ({ id, fetchPerson, isLoading }) => {
  useEffect(() => {
    const fetchPersonInfo = () => {
      fetchPerson(id);
    };
    fetchPersonInfo();
  }, [fetchPerson, id]);
  if (isLoading) {
    return <Loading />;
  } else
    return (
      <div className="person-container">
        <PersonDetails />
      </div>
    );
};
const mapStateToProps = ({ person }) => ({
  isLoading: person.isLoading
});
const mapDispatchToProps = dispatch => ({
  fetchPerson: id => dispatch(fetchPerson(id))
});
export default connect(mapStateToProps, mapDispatchToProps)(PersonPanel);
