import React from "react";
import facebook from "../../images/facebook.png";
import twitter from "../../images/twitter.png";
import instagram from "../../images/instagram.png";
import imdb from "../../images/imdb.png";

const PersonSocial = ({
  social: { twitter_id, instagram_id, facebook_id, imdb_id }
}) => {
  const handleSocialClick = url => {
    var win = window.open(url, "_blank");
    win.focus();
  };

  return (
    <div className="person-social">
      {facebook_id && (
        <img
          onClick={() =>
            handleSocialClick("https://www.facebook.com/" + facebook_id)
          }
          src={facebook}
          alt="F"
        />
      )}
      {twitter_id && (
        <img
          onClick={() => handleSocialClick("https://twitter.com/" + twitter_id)}
          src={twitter}
          alt="F"
        />
      )}
      {instagram_id && (
        <img
          onClick={() =>
            handleSocialClick("https://www.instagram.com/" + instagram_id)
          }
          src={instagram}
          alt="F"
        />
      )}
      {imdb_id && (
        <img
          onClick={() =>
            handleSocialClick("https://www.imdb.com/name/" + imdb_id)
          }
          src={imdb}
          alt="F"
        />
      )}
    </div>
  );
};
export default PersonSocial;
