import React from "react";
import SearchInput from "./SearchInput";

const SearchBar = () => {
  return (
    <div className="main_search">
      <SearchInput />
    </div>
  );
};
export default SearchBar;
