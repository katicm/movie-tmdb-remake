import React, { Component } from "react";
import { connect } from "react-redux";
import { searchTMDb } from "../../movieAPI/actions";
import {
  setSearchTMDb,
  setSearchParameters
} from "../../redux/movie/movie.action";
import SearchDropdown from "./SearchDropdown";
import arrow from "../../images/arrow_down.png";
import "./search.style.scss";

class SearchInput extends Component {
  state = { query: "" };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value }, () =>
      this.searchMovies()
    );
  };
  searchMovies = () => {
    const { query } = this.state;
    const {
      searchParameters: { type },
      setSearchTMDb
    } = this.props;
    if (query === "") {
      setSearchTMDb([]);
      return;
    }
    const data = searchTMDb({ type, query });
    setTimeout(() => setSearchTMDb(data), 500);
  };

  render() {
    const { query } = this.state;
    const {
      searchParameters: { name },
      setSearchParameters
    } = this.props;
    return (
      <div className="search">
        <div className="search_box">
          <input
            className="search_input"
            type="text"
            name="query"
            placeholder="Search..."
            value={query}
            onChange={this.handleChange}
          />
          <SearchDropdown />
        </div>
        <div className="search_type">
          <div className="type">
            {name}
            <img src={arrow} alt="^" />
          </div>
          <div className="type_menu">
            <p
              onClick={() =>
                setSearchParameters({ name: "Movies", type: "movie" })
              }
            >
              Movies
            </p>
            <p
              onClick={() =>
                setSearchParameters({ name: "TV Series", type: "tv" })
              }
            >
              TV Series
            </p>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setSearchTMDb: data => dispatch(setSearchTMDb(data)),
  setSearchParameters: data => dispatch(setSearchParameters(data))
});
const mapStateToProps = ({ movie: { searchParameters } }) => ({
  searchParameters: searchParameters
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchInput);
