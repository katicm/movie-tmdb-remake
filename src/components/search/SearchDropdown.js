import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import not_found from "../../images/not_found.png";

const SearchDropdown = ({ results }) => {
  return (
    <div className="dropdown">
      {results && (
        <div style={{ overflowY: "auto", maxHeight: "500px" }}>
          {results.map(movie => (
            <Link to={`/title/${movie.id}`} key={movie.id}>
              <div className="dropdown_item">
                {movie.poster !== null ? (
                  <img
                    src={"https://image.tmdb.org/t/p/w92/" + movie.poster}
                    alt="X"
                  />
                ) : (
                  <img src={not_found} alt="X" />
                )}
                <p>{movie.name} </p>
                <p>
                  {movie.release_date &&
                    "(" + movie.release_date.split("-")[0] + ")"}
                </p>
              </div>
            </Link>
          ))}
        </div>
      )}
    </div>
  );
};
const mapStateToProps = state => ({
  results: state.movie.results
});
export default connect(mapStateToProps)(SearchDropdown);
