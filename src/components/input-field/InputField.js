import React from "react";

import "./input-field.style.scss";

const InputField = ({ handleChange, label, ...props }) => {
  return (
    <div className="custom_input">
      <input onChange={handleChange} {...props} />
      <label className={props.value.length ? "shrink" : ""}>{label}</label>
    </div>
  );
};
export default InputField;
