import React from "react";
import PopularTitles from "./PopularTitles";
import "./home.style.scss";

const HomeParts = () => {
  document.title = "“Life is a Box of Chocolates” - KMDb";
  window.scrollTo(0, 0);
  return (
    <div className="home">
      <div className="popular">
        <div className="title">Most Popular Movies </div>
        <PopularTitles type="movie" />
      </div>
      <div className="popular">
        <div className="title">Most Popular TV </div>
        <PopularTitles type="tv" />
      </div>
    </div>
  );
};
export default HomeParts;
