import React, { useState, useEffect } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { setSearchParameters } from "../../redux/movie/movie.action";
import arrow from "../../images/arrow_menu.png";
import { Link } from "react-router-dom";

const PopularTitles = ({ type, setSearchParameters }) => {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [indexMovie, setIndexMovie] = useState(0);

  const name = type === "tv" ? "TV Series" : "Movies";
  const focusMovies = data.slice(indexMovie, indexMovie + 3);

  const handlePopular = newIndex => {
    if (newIndex > data.length - 4) setPage(page + 1);
    if (newIndex >= 0) setIndexMovie(newIndex);
  };

  useEffect(() => {
    const fetchPopular = async () => {
      await axios
        .get(
          `https://api.themoviedb.org/3/${type}/popular?api_key=${process.env.REACT_APP_TMDB_API_KEY}&page=${page}`
        )
        .then(result => {
          setData(data => data.concat(result.data.results));
        });
    };
    fetchPopular();
  }, [page, type]);
  return (
    <div className="list">
      <button onClick={() => handlePopular(indexMovie - 1)}>
        <img style={{ transform: "rotate(180deg)" }} src={arrow} alt="<" />
      </button>
      {focusMovies.map(item => (
        <div key={item.id} className="item_movie">
          <Link
            onClick={() => setSearchParameters({ type, name })}
            to={`/title/${item.id}`}
          >
            <img
              src={"https://image.tmdb.org/t/p/w342" + item.poster_path}
              alt="Loading..."
            />
            {type === "tv" ? item.original_name : item.title}
          </Link>
        </div>
      ))}
      <button onClick={() => handlePopular(indexMovie + 1)}>
        <img src={arrow} alt=">" />
      </button>
    </div>
  );
};
const mapDispatchToProps = dispatch => ({
  setSearchParameters: data => dispatch(setSearchParameters(data))
});
export default connect(
  null,
  mapDispatchToProps
)(PopularTitles);
