import React from "react";
import { Link } from "react-router-dom";
import SearchInput from "../search/SearchInput";
import logo from "../../images/logo.png";
import "./header.style.scss";
import Account from "./Account";

const Header = () => {
  return (
    <div className="header">
      <div className="nav_bar">
        <Link to="/">
          <img src={logo} alt="L" />
        </Link>
        <SearchInput />
        <Account />
      </div>
    </div>
  );
};

export default Header;
