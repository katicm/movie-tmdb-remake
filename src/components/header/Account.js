import React from "react";
import { Link } from "react-router-dom";
import { auth } from "../../firebase/firebase.utils";
import { connect } from "react-redux";
import arrow from "../../images/arrow_down_w.png";

const Account = ({ currentUser, watchlistLength }) => {
  return (
    <div className="account">
      {currentUser !== null ? (
        <div className="user">
          <div className="user_drop">
            <Link to="/watchlist">
              <p>Watchlist ({watchlistLength})</p>
            </Link>
            <p onClick={() => auth.signOut()}>Sign out</p>
          </div>
          <div className="username">
            {currentUser.displayName}
            <img src={arrow} alt=">" />
          </div>
        </div>
      ) : (
        <Link to="/login">
          <button>SIGN IN</button>
        </Link>
      )}
    </div>
  );
};
const mapStateToProps = state => ({
  currentUser: state.user.currentUser,
  watchlistLength: state.watchlist.watchlist.length
});

export default connect(mapStateToProps)(Account);
