import axios from "axios";
import { firestore } from "../firebase/firebase.utils";
import firebase from "firebase/app";

export const searchTMDb = param => {
  const name = param.type === "tv" ? "name" : "title";
  const release_date = param.type === "tv" ? "first_air_date" : "release_date";
  const searchResult = [];
  axios
    .get(
      `https://api.themoviedb.org/3/search/${param.type}?api_key=${process.env.REACT_APP_TMDB_API_KEY}&query=${param.query}&page=1&include_adult=true`
    )
    .then(result => {
      result.data.results.map(item =>
        searchResult.push({
          id: item.id,
          name: item[name],
          poster: item.poster_path,
          release_date: item[release_date]
        })
      );
    });
  return searchResult;
};

export const updateWatchlist = (user_id, data, add) => {
  const fieldValue = add
    ? firebase.firestore.FieldValue.arrayUnion
    : firebase.firestore.FieldValue.arrayRemove;
  firestore
    .collection("users")
    .doc(user_id)
    .update({
      watchlist: fieldValue(data)
    });
};

export const getDataToWatchlist = (user_id, data, type, add) => {
  axios
    .get(
      `https://api.themoviedb.org/3/${type}/${data.id}?api_key=${process.env.REACT_APP_TMDB_API_KEY}`
    )
    .then(res => {
      const data =
        type === "tv" ? selectSeriesData(res.data) : selectMovieData(res.data);
      updateWatchlist(user_id, { ...data, added: new Date() }, add);
    })
    .catch(err => console.log(err));
};

export const checkWatchlist = (movie, watchlist) => {
  return watchlist.some(
    item => item.id === movie.id && item.type === movie.type
  );
};

export const selectGenres = list => {
  return list.map(item => item.name);
};

export const sortWatchlist = watchlist => {
  watchlist.sort((prev, next) =>
    prev.added.seconds > next.added.seconds
      ? 1
      : next.added.seconds > prev.added.seconds
      ? -1
      : 0
  );
  return { watchlist };
};

export const selectMovieData = data => {
  return {
    id: data.id,
    title: data.title,
    release_date: data.release_date,
    overview: data.overview,
    poster: data.poster_path,
    rating: data.vote_average,
    genres: selectGenres(data.genres),
    runtime: data.runtime,
    status: data.status,
    budget: data.budget,
    revenue: data.revenue,
    backdrop: data.backdrop_path,
    user_rating: 0,
    type: "movie",
    name: "Movies"
  };
};

export const selectSeriesData = data => {
  return {
    id: data.id,
    title: data.name,
    genres: selectGenres(data.genres),
    last_episode: data.last_air_date,
    runtime: data.episode_run_time[0],
    seasons_num: data.number_of_seasons,
    episodes_num: data.number_of_episodes,
    overview: data.overview,
    rating: data.vote_average,
    poster: data.poster_path,
    air_date: data.first_air_date,
    ended: data.in_production,
    backdrop: data.backdrop_path,
    user_rating: 0,
    type: "series",
    name: "TV Series"
  };
};
