import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { auth, createUserProfileDocument } from "./firebase/firebase.utils";
import { connect } from "react-redux";
import Header from "./components/header/Header";
import HomePage from "./pages/homepage/HomePage";
import LoginPage from "./pages/loginpage/LoginPage";
import MoviePage from "./pages/moviepage/MoviePage";
import WatchlistPage from "./pages/watchlistpage/WatchlistPage";
import PersonPage from "./pages/personpage/PersonPage";
import { setCurrentUser } from "./redux/user/user.action";
import { setCurrentUserWatchlist } from "./redux/watchlist/watchlist.action";
import { sortWatchlist } from "./movieAPI/actions";
import "./App.scss";

class App extends Component {
  unsubscribeFromAuth = null;
  componentDidMount() {
    const { setCurrentUser, setCurrentUserWatchlist } = this.props;
    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      //!sign out
      if (userAuth) {
        //returns userRef from firebase.utils
        const userRef = await createUserProfileDocument(userAuth);
        //get data  ~onChange
        userRef.onSnapshot(snapShot => {
          const { currentUser } = this.props;
          const { createdAt, displayName, email, watchlist } = snapShot.data();
          if (currentUser === null || currentUser.id !== snapShot.id) {
            setCurrentUser({
              id: snapShot.id,
              createdAt,
              displayName,
              email
            });
          }
          setCurrentUserWatchlist(sortWatchlist(watchlist));
        });
      } else {
        setCurrentUser(userAuth);
        setCurrentUserWatchlist({ watchlist: [] });
      } //log out set to null
    });
  }
  componentWillUnmount() {
    this.unsubscribeFromAuth();
  }

  render() {
    return (
      <div className="App">
        <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route
            exact
            path="/login"
            render={() =>
              this.props.currentUser ? <Redirect to="/" /> : <LoginPage />
            }
          />
          <Route path="/title/:titleID" component={MoviePage} />
          <Route
            path="/watchlist"
            render={() =>
              this.props.currentUser ? <WatchlistPage /> : <Redirect to="/" />
            }
          />
          <Route path="/person/:personID" component={PersonPage} />
        </Switch>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user)),
  setCurrentUserWatchlist: watchlist =>
    dispatch(setCurrentUserWatchlist(watchlist))
});
const mapStateToProps = ({ user }) => ({
  currentUser: user.currentUser
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

//props.history/location/match.url
