import * as movieAction from "./movie.types";

const INITIAL_STATE = {
  results: false,
  searchParameters: { name: "Movies", type: "movie" },
  movie: { id: null },
  isLoading: true,
  error: undefined
};

const movieReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case movieAction.SET_SEARCH_TMDB: {
      return {
        ...state,
        results: action.payload
      };
    }
    case movieAction.SET_SEARCH_PARAMETERS: {
      return { ...state, searchParameters: action.payload, results: false };
    }
    case movieAction.FETCH_MOVIE_START: {
      return { ...state, isLoading: true, error: undefined };
    }
    case movieAction.SET_FETCHED_MOVIE: {
      return { ...state, movie: action.payload, isLoading: false };
    }
    case movieAction.SET_FETCHED_MOVIE_FAILURE: {
      return { ...state, error: action.payload, isLoading: false };
    }
    default:
      return state;
  }
};
export default movieReducer;
