import * as movieAction from "./movie.types";

export const setSearchTMDbResult = data => ({
  type: movieAction.SET_SEARCH_TMDB,
  payload: data
});

export const setSearchParameters = data => ({
  type: movieAction.SET_SEARCH_PARAMETERS,
  payload: data
});

export const setSearchTMDb = data => {
  return dispatch => {
    if (data.length !== 0) {
      dispatch(setSearchTMDbResult(data));
    }
  };
};

export const fetchMovie = (type, id) => {
  return {
    type: movieAction.FETCH_MOVIE_START,
    searchType: type,
    id: id
  };
};

export const setFetchedMovie = payload => {
  return {
    type: movieAction.SET_FETCHED_MOVIE,
    payload
  };
};

export const setFetchedMovie_Failure = payload => {
  return {
    type: movieAction.SET_FETCHED_MOVIE_FAILURE,
    payload
  };
};
