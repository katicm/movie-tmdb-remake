import { put } from "redux-saga/effects";
import { setFetchedMovie, setFetchedMovie_Failure } from "./movie.action";
import { selectMovieData, selectSeriesData } from "../../movieAPI/actions";
import axios from "axios";

export function* fetchMovieSaga(action) {
  try {
    const response = yield axios.get(
      `https://api.themoviedb.org/3/${action.searchType}/${action.id}?api_key=${process.env.REACT_APP_TMDB_API_KEY}`
    );
    yield put(
      setFetchedMovie(
        action.searchType === "tv"
          ? selectSeriesData(response.data)
          : selectMovieData(response.data)
      )
    );
  } catch (error) {
    yield put(setFetchedMovie_Failure(error));
  }
}
