import { WatchlistActionTypes } from "./watchlist.types";

const INITIAL_STATE = {
  watchlist: []
};

const watchlistReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case WatchlistActionTypes.SET_CURRENT_WATCHLIST:
      return action.payload;
    default:
      return state;
  }
};

export default watchlistReducer;
