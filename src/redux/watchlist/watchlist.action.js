import { WatchlistActionTypes } from "./watchlist.types";

export const setCurrentUserWatchlist = watchlist => ({
  type: WatchlistActionTypes.SET_CURRENT_WATCHLIST,
  payload: watchlist
});
