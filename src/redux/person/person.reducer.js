import * as personAction from "./person.types";

const personReducer = (state = { isLoading: true }, action) => {
  switch (action.type) {
    case personAction.FETCH_PERSON_START: {
      return { ...state, isLoading: true };
    }
    case personAction.SET_FETCHED_PERSON_DETAILS:
    case personAction.SET_FETCHED_PERSON_PHOTOS:
    case personAction.SET_FETCHED_PERSON_SOCIAL: {
      return {
        ...state,
        person: { ...state.person, ...action.payload }
      };
    }
    case personAction.SET_FETCHED_PERSON_CREDITS:
      return {
        ...state,
        person: { ...state.person, ...action.payload },
        isLoading: false
      };
    case personAction.SET_FETCHED_PERSON_FAILURE: {
      return { ...state, error: action.payload, isLoading: false };
    }
    default:
      return state;
  }
};
export default personReducer;
