import * as personAction from "./person.types";

export const fetchPerson = id => ({
  type: personAction.FETCH_PERSON_START,
  id
});

export const setFetchedPersonDetails = payload => ({
  type: personAction.SET_FETCHED_PERSON_DETAILS,
  payload
});

export const setFetchedPersonCredits = payload => ({
  type: personAction.SET_FETCHED_PERSON_CREDITS,
  payload
});

export const setFetchedPersonPhotos = payload => ({
  type: personAction.SET_FETCHED_PERSON_PHOTOS,
  payload
});

export const setFetchedPersonSocial = payload => ({
  type: personAction.SET_FETCHED_PERSON_SOCIAL,
  payload
});

export const setFetchedPersonFailure = payload => ({
  type: personAction.SET_FETCHED_PERSON_FAILURE,
  payload
});
