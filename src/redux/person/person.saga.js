import { put, all, fork } from "redux-saga/effects";
import {
  setFetchedPersonDetails,
  setFetchedPersonFailure,
  setFetchedPersonCredits,
  setFetchedPersonPhotos,
  setFetchedPersonSocial
} from "./person.action";
import axios from "axios";

export function* fetchPersonSaga(action) {
  try {
    yield all([
      fork(
        fetchAndDispatch,
        `https://api.themoviedb.org/3/person/${action.id}?api_key=${process.env.REACT_APP_TMDB_API_KEY}&language=en-US`,
        setFetchedPersonDetails
      ),
      fork(
        fetchAndDispatch,
        `https://api.themoviedb.org/3/person/${action.id}/images?api_key=${process.env.REACT_APP_TMDB_API_KEY}&language=en-US`,
        setFetchedPersonPhotos
      ),
      fork(
        fetchAndDispatch,
        `https://api.themoviedb.org/3/person/${action.id}/external_ids?api_key=${process.env.REACT_APP_TMDB_API_KEY}&language=en-US`,
        setFetchedPersonSocial
      ),
      fork(
        fetchAndDispatch,
        `https://api.themoviedb.org/3/person/${action.id}/combined_credits?api_key=${process.env.REACT_APP_TMDB_API_KEY}&language=en-US`,
        setFetchedPersonCredits
      )
    ]);
  } catch (error) {
    put(setFetchedPersonFailure(error));
  }
}

function* fetchAndDispatch(id, actionType) {
  const response = yield axios.get(id);
  yield put(actionType(response.data));
}
