import { combineReducers } from "redux";
import userReducer from "./user/user.reducer";
import movieReducer from "./movie/movie.reducer";
import watchlistReducer from "./watchlist/watchlist.reducer";
import personReducer from "./person/person.reducer";

export default combineReducers({
  user: userReducer,
  movie: movieReducer,
  watchlist: watchlistReducer,
  person: personReducer
});
