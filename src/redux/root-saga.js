import { takeEvery } from "redux-saga/effects";
import * as movieAction from "./movie/movie.types";
import * as personAction from "./person/person.types";
import { fetchMovieSaga } from "./movie/movie.saga";
import { fetchPersonSaga } from "./person/person.saga";

export function* watchMovie() {
  yield takeEvery(movieAction.FETCH_MOVIE_START, fetchMovieSaga);
}

export function* watchPerson() {
  yield takeEvery(personAction.FETCH_PERSON_START, fetchPersonSaga);
}
